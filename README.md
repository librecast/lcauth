# lcauth.js

Javascript library to access Librecast Authentication over Multicast from the
web browser.

See `test/` directory for example usage.

## License

This work is dual-licensed under GPL 2.0 and GPL 3.0.

SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

Copyright © 2020 Brett Sheffield <bacs@librecast.net>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (see the file COPYING in the distribution).
If not, see <http://www.gnu.org/licenses/>.
